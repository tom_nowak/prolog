%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%high-level trace for demonstration purposes:

log_on_failure(ProcedureWithArgs, _, _) :-
    ProcedureWithArgs, !. % backtracking in ProcedureWithArgs is impossible 

log_on_failure(ProcedureWithArgs, Message, Depth) :-
    write_number_as_indentation(Depth), write(Message), write(' : '), write(ProcedureWithArgs), write(' failed'), nl.


log_after_success(ProcedureWithArgs, Depth) :-
    ProcedureWithArgs,
    write_number_as_indentation(Depth), write(ProcedureWithArgs), nl.


write_number_as_indentation(Number) :-
    Number =< 0, !.

write_number_as_indentation(Number) :-
    Number > 0,
    write('   '),
    Number1 is Number - 1,
    write_number_as_indentation(Number1).


block_on_input :-
    write('input anything to continue... '),
    catch(read(_), error(_,_Context), (true)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%main procedure - plan(InitState, Goals, Plan, FinalState, Interactive, Trace)

plan(InitState, Goals, Plan, FinalState, Interactive, 1) :-
    write('starting main plan with State '), write(InitState), write(', Goals '), write(Goals), nl,
    log_after_success(generate_number_infinite(1, Limit), 0),
    plan_private(InitState, Goals, [], Limit, Plan, FinalState, Interactive, 1, 1).

plan(InitState, Goals, Plan, FinalState, Interactive, 0) :-
    generate_number_infinite(1, Limit),
    plan_private(InitState, Goals, [], Limit, Plan, FinalState, Interactive, 1, 0).


plan_private(State, Goals, _, _, [], State, _, RecursionDepth, 1) :-
    write_number_as_indentation(RecursionDepth), write('plan_private/1 with Goals '), write(Goals), nl,
    log_after_success(goals_achieved(Goals, State), RecursionDepth).

plan_private(InitState, Goals, AchievedGoals, Limit, Plan, FinalState, Interactive, RecursionDepth, 1) :-
    write_number_as_indentation(RecursionDepth), write('plan_private/2 with State '), write(InitState), write(', Goals '),
    write(Goals), write(', AchievedGoals '), write(AchievedGoals), write(', Limit '), write(Limit), nl,
    %block_on_input,
    log_on_failure((Limit > 0), 'depth limit exceeded', RecursionDepth),
    log_after_success(generate_number(0, Limit, LimitPre), RecursionDepth),
    log_after_success(choose_goal(Goal, Goals, RestGoals, InitState), RecursionDepth),
    log_after_success(achieves(Goal, Action), RecursionDepth),
    write_number_as_indentation(RecursionDepth), write('trying to achieve '), write(Goal), write(' with '), write(Action), nl,
    log_after_success(requires(Action, CondGoals, Conditions), RecursionDepth),
    RecursionDepth1 is RecursionDepth + 1,
    write_number_as_indentation(RecursionDepth), write('starting preplan\n'),
    plan_private(InitState, CondGoals, AchievedGoals, LimitPre, PrePlan, State1, Interactive, RecursionDepth1, 1),
    log_after_success(inst_action(Action, Conditions, State1, InstAction, Interactive), RecursionDepth),
    write_number_as_indentation(RecursionDepth), check_action(InstAction, AchievedGoals, 1),
    log_after_success(perform_action(State1, InstAction, State2), RecursionDepth),
    LimitPost is Limit - LimitPre - 1,
    write_number_as_indentation(RecursionDepth), write('starting postplan\n'),
    plan_private(State2, RestGoals, [Goal | AchievedGoals], LimitPost, PostPlan, FinalState, Interactive, RecursionDepth1, 1),
    log_after_success(conc(PrePlan, [InstAction | PostPlan], Plan), RecursionDepth).

plan_private(State, Goals, _, _, [], State, _, _, 0) :-
    goals_achieved(Goals, State).

plan_private(InitState, Goals, AchievedGoals, Limit, Plan, FinalState, Interactive, _, 0) :-
    Limit > 0,
    generate_number(0, Limit, LimitPre),
    choose_goal(Goal, Goals, RestGoals, InitState),
    achieves(Goal, Action),
    requires(Action, CondGoals, Conditions),
    plan_private(InitState, CondGoals, AchievedGoals, LimitPre, PrePlan, State1, Interactive, _, 0),
    inst_action(Action, Conditions, State1, InstAction, Interactive),
    check_action(InstAction, AchievedGoals, Interactive),
    perform_action(State1, InstAction, State2),
    LimitPost is Limit - LimitPre - 1,
    plan_private(State2, RestGoals, [Goal | AchievedGoals], LimitPost, PostPlan, FinalState, Interactive, _, 0),
    conc(PrePlan, [InstAction | PostPlan], Plan).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%helper procedures (problem-specific):

%goals_achieved(Goals, State):

goals_achieved([], _).

goals_achieved([Goal | RemainingGoals], State) :-
    goal_achieved(Goal, State),
    goals_achieved(RemainingGoals, State).


%goal_achieved(Goal, State):

goal_achieved(clear(Object), State) :-
    (Object \= _/_; var(Object)),
    member(clear(Object), State).

goal_achieved(on(Block, Object), State) :-
    (Object \= _/_; var(Object)),
    (Block \= _/_; var(Block)),
    member(on(Block, Object), State).

goal_achieved(clear(Block/Condition), State) :-
    nonvar(Condition),
    goal_achieved(Condition, State),
    member(clear(Block), State).

goal_achieved(on(Block, Object/Condition), State) :-
    nonvar(Condition),
    goal_achieved(Condition, State),
    member(on(Block, Object), State).

goal_achieved(diff(Object1, Object2), _) :-
    Object1 \= _/_,
    Object2 \= _/_,
    Object1 \= Object2.

goal_achieved(diff(Object1/Condition, Object2), State) :-
    nonvar(Condition),
    goal_achieved(Condition, State),
    Object1 \= Object2.

goal_achieved(diff(Object1, Object2/Condition), State) :-
    nonvar(Condition),
    goal_achieved(Condition, State),
    Object1 \= Object2.


%choose_goal(Goal, Goals, RestGoals, InitState):

choose_goal(Goal, Goals, RestGoals, InitState) :-
    delete1(Goal, Goals, RestGoals),
    \+(goal_achieved(Goal, InitState)).


%achieves(Goal, Action):

achieves(on(Object, Target), move(Object, X/on(Object, X), Target)).

achieves(clear(Object), move(X/on(X, Object), Object, _)).


%requires(Action, CondGoals, Conditions):

requires(move(Object, X/on(Object, X), Target), [clear(Object), clear(Target)], [on(Object, X)]).

requires(move(X/on(X, Object), Object, Y), [clear(X/on(X, Object))], [clear(Y), diff(Y, X/on(X, Object))]).

requires(move(Object, From, Target), [clear(Object), clear(Target), on(Object, From)], []) :-
    Object \= _/_,
    From \= _/_,
    Target \= _/_.


%inst_action(Action, Conditions, State1, InstAction, Interactive):

%non-interactive
inst_action(move(X, Y, Z), Conditions, State1, move(InstantiatedX, InstantiatedY, Z), 0) :-
    goals_achieved(Conditions, State1),
    remove_conditions(X, InstantiatedX),
    remove_conditions(Y, InstantiatedY).


%interactive
inst_action(move(X, Y, Z), Conditions, State1, move(InstantiatedX, InstantiatedY, Z), 1) :-
    nonvar(Z),
    inst_action(move(X, Y, Z), Conditions, State1, move(InstantiatedX, InstantiatedY, Z), 0),
    write('\ntrying to perform action '), write(move(InstantiatedX, InstantiatedY, Z)), write(' (move target already instantiated)\n'),
    interactive_confirm_instantiated(Z, State1).

inst_action(move(X, Y, Z), Conditions, State1, move(InstantiatedX, InstantiatedY, Z), 1) :-
    var(Z),
    remove_conditions(X, InstantiatedX),
    remove_conditions(Y, InstantiatedY),
    write('\ntrying to perform action move('), write(InstantiatedX), write(','), write(InstantiatedY), write(',?)\n'),
    interactive_instantiate(Conditions, Z, State1, _).


interactive_confirm_instantiated(MoveTarget, State) :-
    write('state: '), write(State), write('\n'),
    user_input([MoveTarget], _, CancelRequest),
    CancelRequest \= 1.


interactive_instantiate([clear(MoveTarget), diff(MoveTarget, Block)], MoveTarget, State, CancelRequest) :-
    %write('calling interactive_instantiate 1 with CancelRequest '), write(CancelRequest), write('\n'), %for 'debugging'
    write('state: '), write(State), write('\n'),
    setof(MoveTarget, goals_achieved([clear(MoveTarget), diff(MoveTarget, Block)], State), AvailablePositions),
    user_input(AvailablePositions, MoveTarget, CancelRequest),
    CancelRequest \= 1.

interactive_instantiate([clear(MoveTarget), diff(MoveTarget, Block)], MoveTarget, State, 0) :-
    %write('calling interactive_instantiate 2\n'), %for 'debugging
    interactive_instantiate([clear(MoveTarget), diff(MoveTarget, Block)], MoveTarget, State, _).


user_input(AvailablePositions, MoveTarget, CancelRequest) :-
    write('please choose position from '), write(AvailablePositions), write(' or write cancel to force backtracking: '),
    catch(read(Input), error(_,_Context), (write('incorrect input\n'), user_input(AvailablePositions, MoveTarget, CancelRequest))),
    ((validate_input(Input, AvailablePositions, MoveTarget, CancelRequest), !);
    (write('incorrect input\n'), user_input(AvailablePositions, MoveTarget, CancelRequest))).


validate_input('cancel', _, _, 1).

validate_input(Input, AvailablePositions, Input, 0) :-
    member(Input, AvailablePositions).


%perform_action(State1, InstAction, State2):

perform_action(State1, move(Block, From, To), [clear(From), on(Block, To) | State2]) :-
    subtract_(State1, [on(Block,From), clear(To)], State2).


%check_action(InstAction, AchievedGoals, InteractiveFlag):

check_action(move(_, _, _), [], 0).

check_action(move(Block, From, To), [], 1) :-
    write('action '), write(move(Block, From, To)),
    write(' has been verified to not destroy any achieved goal\n').

check_action(move(Block, From, To), [AchievedGoal | RestAchievedGoals], InteractiveFlag) :-
    \+(action_destroys_goal(move(Block, From, To), AchievedGoal, InteractiveFlag)),
    check_action(move(Block, From, To), RestAchievedGoals, InteractiveFlag).


action_destroys_goal(move(Block, From, To), AchievedGoal, 0) :-
    AchievedGoal == on(Block,From); AchievedGoal == clear(To).

action_destroys_goal(move(Block, From, To), AchievedGoal, 1) :-
    (AchievedGoal == on(Block,From); AchievedGoal == clear(To)),
    write('action '), write(move(Block, From, To)),
    write(' would destroy achieved goal '), write(AchievedGoal), write('\n').


%remove_conditions(ObjectWithConditions, ObjectWithoutConditions):

remove_conditions(Object, Object) :-
    Object \= _/_.

remove_conditions(Object/_, Object) :-
    Object \= _/_.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%helper procedures (general-purpose):

%conc(List1, List2, Result):

conc([], List2, List2).

conc([Head | Rest1], List2, [Head | RestResult]):-
     conc(Rest1, List2, RestResult).


%delete1(Element, List, Result):

delete1(Element, [Element | Rest], Rest).

delete1(Element, [Head | Rest], [Head | RestResult]) :-
    delete1(Element, Rest, RestResult).


%subtract_(InputList, SubtractedList, OutputList):

subtract_([], _, []).

subtract_([Head | RestList], Subtracted, Output) :-
    member(Head, Subtracted), !,
    subtract_(RestList, Subtracted, Output).

subtract_([Head | RestList], Subtracted, [Head | Output]) :-
    subtract_(RestList, Subtracted, Output).


%sets_equal(Set1, Set2):

sets_equal(Set1, Set2) :-
    subtract_(Set1, Set2, []),
    subtract_(Set2, Set1, []).


%generate_number(Min, Limit, Result):

generate_number(Min, Limit, Min) :-
    Min < Limit.

generate_number(Min, Limit, Result) :-
    Min1 is Min + 1,
    Min1 < Limit,
    generate_number(Min1, Limit, Result).


%generate_number_infinite(Min, Result):
generate_number_infinite(Result, Result).

generate_number_infinite(Min, Result) :-
    Min1 is Min + 1,
    generate_number_infinite(Min1, Result).


%conditional_write(Message, Flag):
conditional_write(_, 0).

conditional_write(Message, 1) :-
    write(Message).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%tests:

tests :-
    test(test_general_purpose_helper_procedures, 'test_general_purpose_helper_procedures '),
    test(test_goals_achieved, 'test_goals_achieved '),
    test(test_goal_achieved_on_clear, 'test_goal_achieved_on_clear '),
    test(test_goal_achieved_diff, 'test_goal_achieved_diff '),
    test(test_choose_goal, 'test_choose_goal '),
    test(test_achieves, 'test_achieves '),
    test(test_requires, 'test_requires '),
    test(test_inst_action, 'test_inst_action '),
    test(test_remove_conditions, 'test_remove_conditions '),
    test(test_perform_action, 'test_perform_action '),
    test(test_check_action, 'test_check_action '),
    test(test_check_plan_steps, 'test_check_plan_steps '),
    test(test_plan, 'test_plan ').

test_interactive_instantiate :-
    write('interactive_instantiate([clear(MoveTarget), diff(MoveTarget, b)], MoveTarget, [on(b,p1), clear(b), clear(p2), clear(p3)])\n'),
    interactive_instantiate([clear(MoveTarget), diff(MoveTarget, b)], MoveTarget, [on(b,p1), clear(b), clear(p2), clear(p3)]),
    write('MoveTarget: '), write(MoveTarget), write('\n').

test_interactive_plan :-
    write('plan([on(b1,p1),on(b2,b1),clear(b2),clear(p2),clear(p3)], [clear(p2), on(b1,b2)], Plan, FinalState, 1, 0)\n'),
    plan([on(b1,p1),on(b2,b1),clear(b2),clear(p2),clear(p3)], [clear(p2), on(b1,b2)], Plan, FinalState, 1, 0),
    write('Plan: '), write(Plan), write('\n'),
    write('FinalState: '), write(FinalState), write('\n').

test(Statement, MsgOnStart) :-
    write(MsgOnStart),
    Statement, !,
    write(' passed\n').

test(_, _) :-
    write(' failed\n'),
    fail.

test_general_purpose_helper_procedures :-
    test_conc, write('.'),
    test_delete1, write('.'),
    test_subtract_, write('.'),
    test_sets_equal, write('.'),
    test_generate_number, write('.').

%conc(List1, List2, Result) test:
test_conc :-
    conc([a, b], [], [a, b]),
    conc([], [c, d, e], [c, d, e]),
    conc([a, b], [c, d, e], [a, b, c, d, e]).

%delete1(Element, List, Result) test:
test_delete1 :-
    delete1(a, [b, a, c, a, d], Output),
    test_delete1_force_backtracking(Output),
    Output == [b, a, c, d].

test_delete1_force_backtracking([b, a, c, d]).

%subtract_(InputList, SubtractedList, OutputList) test:
test_subtract_ :-
    subtract_([a, b, c, d], [a, c, e], [b, d]).

%sets_equal(Set1, Set2) test:
test_sets_equal :-
    sets_equal([], []),
    sets_equal([a, b, c], [b, c, a]),
    \+(sets_equal([a, b], [a, b, c])),
    \+(sets_equal([a, b, c], [a, b])).

%generate_number(Min, Limit, Result) test:
test_generate_number :-
    test_generate_number1,
    test_generate_number3,
    \+(test_generate_number_limit).

test_generate_number1 :-
    generate_number(0, 4, X), X == 1.

test_generate_number3 :-
    generate_number(0, 4, X), X == 3.

test_generate_number_limit :-
    generate_number(0, 4, X), X == 4.

%generate_number_infinite(Min, Result) - tested manually

%goals_achieved(Goals, State) test:
test_goals_achieved :-
    goals_achieved([], []), write('.'),
    goals_achieved([on(b2, b1), clear(p2)], [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)]), write('.'),
    \+(goals_achieved([on(b2, b1), clear(p2)], [on(b1, p1), on(b2, b1), clear(b2), clear(p3)])), write('.').

%goal_achieved(Goal, State) tests:
test_goal_achieved_on_clear :-
    goal_achieved(clear(p2), [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)]), write('.'),
    goal_achieved(on(b2, b1), [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)]), write('.'),
    goal_achieved(clear(X1/on(X1, Y1/on(Y1, p1))), [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)]), !, X1 == b2, Y1 == b1, write('.'),
    goal_achieved(on(b2, X2/on(X2, p1)), [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)]), !, X2 == b1, write('.').

test_goal_achieved_diff :-
    goal_achieved(diff(b1, b2), []), write('.'),
    goal_achieved(diff(X1/on(b2, Y1/on(Y1, X1)), p2), [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p1)]), !, X1 == p1, Y1 == b1, write('.'),
    goal_achieved(diff(p1, X2/clear(X2)), [clear(p1), clear(p2)]), !, X2 == p2, write('.'),
    \+(goal_achieved(diff(_, _), [])), write('.'),  % non instantiated arguments - goal_achieved(diff(...)) fails
    \+(goal_achieved(diff(_,a), [])), write('.').  % non instantiated argument - goal_achieved(diff(...)) fails

%choose_goal(Goal, Goals, RestGoals, InitState) test:
test_choose_goal :-
    choose_goal(Goal, [clear(p2), clear(p1), on(b1, b2)], RestGoals, [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)]), !,
    Goal == clear(p1), RestGoals == [clear(p2), on(b1, b2)], write('.'),
    \+(choose_goal(_, [clear(p2)], _, [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)])), write('.'),
    test_choose_goal_with_forced_backtracking, write('.').

test_choose_goal_with_forced_backtracking :-
    choose_goal(Goal, [clear(p1), on(b1, b2)], RestGoals, [on(b1, p1), on(b2, b1), clear(b2), clear(p2), clear(p3)]),
    filter_out_goal(Goal),
    Goal == on(b1, b2),
    RestGoals == [clear(p1)].

filter_out_goal(on(_, _)).

%achieves(Goal, Action) test:
test_achieves :-
    achieves(on(b1, b2), move(b1, X1/on(b1, X1), b2)), write('.'),
    achieves(clear(b1), move(X2/on(X2, b1), b1, _)), write('.').

%requires(Action, CondGoals, Conditions) test:
test_requires :-
    requires(move(b1, X1/on(b1, X1), b2), [clear(b1), clear(b2)], [on(b1, X1)]), write('.'),
    requires(move(X2/on(X2, b1), b1, Y2), [clear(X2/on(X2, b1))], [clear(Y2), diff(Y2, X2/on(X2, b1))]), write('.'),
    requires(move(b1, p1, p2), [clear(b1), clear(p2), on(b1, p1)], []), write('.').

%inst_action(Action, Conditions, State1, InstAction) test:
test_inst_action :-
    inst_action(move(b1, X1/on(b1, X1), b2), [on(b1, X1)], [on(b1, p1), clear(b1), on(b2, p2), clear(b2)], InstAction1, 0), !,
    InstAction1 == move(b1, p1, b2), write('.'),
    inst_action(move(X2/on(X2, b1), b1, Y2), [clear(Y2), diff(Y2, X2/on(X2, b1))], [on(b2, b1), clear(b2), clear(p2)], InstAction2, 0), !,
    InstAction2 == move(b2, b1, p2), write('.').

%remove_conditions(ObjectWithConditions, ObjectWithoutConditions) test:
test_remove_conditions :-
    remove_conditions(b1, X1), !, X1 == b1, write('.'),
    remove_conditions(b1/on(b1, p1), X2), !, X2 == b1, write('.').

%perform_action(State1, InstAction, State2) test:
test_perform_action :-
    perform_action([on(b2, b1), on(b1, p1), clear(b2), clear(p2)], move(b2, b1, p2), FinalState), !,
    sets_equal(FinalState, [on(b1, p1), clear(b1), on(b2, p2), clear(b2)]),
    write('.').

%check_action(InstAction, AchievedGoals) test:
test_check_action :-
    check_action(move(b1, p1, p2), [clear(p3), clear(p4)], 0), write('.'),
    \+(check_action(move(b1, p1, p2), [clear(p3), on(b1, p1), clear(p4)], 0)), write('.'),
    \+(check_action(move(b1, p1, p2), [clear(p3), clear(p2), clear(p4)], 0)), write('.').

%plan(InitState, Goals, Plan, FinalState, Interactive, Trace) test:
test_plan :-
    test_simple_plan,
    test_complex_plan([on(b4, p1), on(b1, b4), on(b3, b1), on(b2, p3), clear(b3), clear(b2), clear(p2), clear(p4)], [on(b4, b2)]).

test_simple_plan :-
    plan([on(b1,p1), clear(b1), clear(p2)], [clear(p1)], Plan, FinalState, 0, 0), !,
    Plan == [move(b1, p1, p2)],
    sets_equal(FinalState, [clear(p1), on(b1,p2), clear(b1)]),
    write('.').

test_complex_plan(InitState, Goals) :-
    plan(InitState, Goals, Plan, FinalState, 0, 0), !,
    check_plan_steps(Plan, InitState, FinalState),
    goals_achieved(Goals, FinalState),
    write('.').

%check_plan_steps(Plan, InitState, FinalState) checks if each action in plan is correct and if Plan transforms InitState into FinalState:
check_plan_steps([], FinalState, FinalState).

check_plan_steps([InstAction | RestPlan], State, FinalState) :-
    requires(InstAction, CondGoals, Conditions),
    goals_achieved(CondGoals, State),
    goals_achieved(Conditions, State),
    perform_action(State, InstAction, State1),
    check_plan_steps(RestPlan, State1, FinalState).

test_check_plan_steps :-
    check_plan_steps([], State, State), write('.'),
    check_plan_steps([move(b1, p1, p2)], [on(b1,p1), clear(b1), clear(p2)], FinalState),
    sets_equal(FinalState, [clear(p1), on(b1,p2), clear(b1)]), write('.').
